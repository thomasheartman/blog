const { makeLinksAbsolute } = require("./sitesrc/utils")
const siteTitle = `Whisper of the Heartman`
const siteUrl = `https://blog.thomasheartman.com`
const rssPath = `/rss.xml`
module.exports = {
  siteMetadata: {
    title: siteTitle,
    author: {
      name: `Thomas Heartman`,
      summary: `Thomas Heartman is a developer, writer, speaker, and one of those odd people who enjoy lifting heavy things and putting them back down again. Preferably with others. Doing his best to gain and share as much knowledge as possible.`,
    },
    description: `Thoughts, tips, and tricks on software development, text editors, and the industry. Expect posts on keyboard layouts, functional programming, type systems, Emacs, Nix(OS), git, Rust, and more.`,
    siteUrl,
    social: {
      twitter: `thomasheartman`,
      openGraphDefaultImage: `/static/opengraph.jpg`,
    },
  },
  plugins: [
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/content/pages`,
        name: `pages`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/content/blog`,
        name: `blog`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/content/assets`,
        name: `assets`,
      },
    },
    {
      resolve: `gatsby-transformer-orga`,
      options: {
        plugins: [
          {
            resolve: `gatsby-remark-prismjs`,
            options: {
              aliases: { sh: "bash" },
              showLineNumbers: true,
            },
          },
        ],
      },
    },
    `gatsby-transformer-sharp`,
    {
      resolve: `gatsby-plugin-sharp`,
      options: {
        useMozJpeg: false,
      },
    },
    {
      resolve: `gatsby-plugin-feed`,
      options: {
        query: `
          {
            site {
              siteMetadata {
                title
                description
                siteUrl
                site_url: siteUrl
              }
            }
          }
        `,
        feeds: [
          {
            serialize: ({ query: { site, posts } }) => {
              return posts.nodes.map(node => {
                const url = `${site.siteMetadata.siteUrl}${node.fields.path}`
                return Object.assign({}, node.metadata, {
                  date: node.metadata.published,
                  categories: node.metadata.tags,
                  url,
                  guid: url,
                  custom_elements: [
                    {
                      "content:encoded": makeLinksAbsolute(siteUrl)(node.html),
                    },
                  ],
                })
              })
            },
            query: `
              {
                posts: allOrgContent(
                  sort: { fields: [metadata___published], order: DESC }
                  filter: { metadata: { published: { ne: null } } }
                ) {
                  nodes {
                    html
                    fields {
                      path
                    }
                    metadata {
                      title
                      description
                      published
                      tags
                    }
                  }
                }
              }
            `,
            output: rssPath,
            title: siteTitle,
            custom_elements: [
              {
                "atom:link": {
                  _attr: {
                    href: `${siteUrl}${rssPath}`,
                    rel: "self",
                    type: "application/rss+xml",
                  },
                },
              },
            ],
          },
        ],
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: siteTitle,
        short_name: `Heartman`,
        start_url: `/`,
        background_color: `#ffffff`,
        theme_color: `#212121`,
        display: `minimal-ui`,
        icon: `content/assets/favicon-gradient.svg`,
      },
    },
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-sitemap`,
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
