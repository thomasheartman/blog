"""
Take posts in the drafts directory, add `published` field, move them to 'posts' directory.
The script assumes that the drafts dir is a direct subdirectory of the posts directory.
"""

import os
import sys

import hakyll
import string_ops


def main(output_dir: str, posts: [str]) -> bool:
    print(f"*Publishing posts*:\n\n{posts}\n\n")

    for p in posts:
        publish(output_dir, p)

    print(f"Successfully published all posts🎉")


def to_url(title: str) -> str:
    char_map = {
        '\'': "",
        ',': "",
        '.': "",
        '!': "",
        '?': "",
        ':': "",
        ';': "",
        '/': "",
        '\\': "",
        ' ': '-'
    }
    return "".join([char_map.get(c, c) for c in title.strip()]).lower()


def publish(output_dir: str, post: str):
    _, extension = os.path.splitext(post)
    post_name = string_ops.strip_surrounding_quotes(
        to_url(hakyll.get_metadata(post)["title"]))
    new_path = os.path.join(output_dir, post_name + extension)
    print(f"Moving {post} to {new_path}.")
    os.rename(post, new_path)
    hakyll.update_metadata_field("published", "", new_path)


if __name__ == "__main__":
    assert len(sys.argv) >= 3, "I need an output dir and a list of posts to publish."

    _, output_dir, *posts = sys.argv
    assert os.path.isdir(output_dir), f"{output_dir} isn't a directory."
    for p in posts:
        assert os.path.isfile(p), f"{p} isn't a file."

    main(output_dir, posts)
