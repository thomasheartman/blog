import os


def changed_posts_since(prev_commit: str) -> [str]:
    changes = changed_files_since(prev_commit)
    return [x for x in changes if x.startswith("posts/")]


def changed_files_since(prev_commit: str) -> [str]:
    return os.popen(f"git diff --name-only {prev_commit} HEAD") \
            .read() \
            .strip() \
            .splitlines()
