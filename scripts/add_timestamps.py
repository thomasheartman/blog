#!/usr/bin/env/python
"""
Make sure that all posts that are marked `published` have a time stamp.
If they do not already, add one.
If any previously published posts have been modified, add a modified field
"""

import os
import re
import sys

import git
import hakyll


def current_timestamp():
    return os.popen("date --utc +%FT%T%:z").read().strip()


def split_files(posts: [str]) -> ({str}, {str}):
    file_set = set(posts)
    new_posts = {f for f in file_set if hakyll.is_new(f)}
    modified_posts = file_set - new_posts
    return (modified_posts, new_posts)


def filter_posts(posts: [str]) -> [str]:
    return [f for f in posts if hakyll.is_published(f)]


def update_timestamp(field: str, timestamp: str, posts: {str}):
    for p in posts:
        hakyll.update_metadata_field(field, timestamp, p)


def main(commit: str):
    changed_posts = [x for x in git.changed_posts_since(commit) if os.path.isfile(x)]

    published_posts = [
        f for f in changed_posts if "published" in hakyll.get_metadata(f)
    ]

    if not published_posts:
        print(
            "I found no posts that have changed since the provided commit. All clear."
        )
        return

    print(
        f"These are all the posts I found that have been changed: {published_posts}"
    )

    modified, new = split_files(published_posts)
    print(f"*Modified posts*:\n{modified or None}")
    print(f"*New posts*:\n{new or None}")

    timestamp = current_timestamp()
    print(f"Using {timestamp} as timestamp")

    if new:
        update_timestamp("published", timestamp, new)
        print(f"Updated {new} as new posts.")

    if modified:
        update_timestamp("modified", timestamp, modified)
        print(f"Updated {modified} as modified posts.")

    print(f"Updated all files. Script terminating.")


if __name__ == "__main__":
    assert len(
        sys.argv
    ) == 2, "I did not get the right amount of arguments. I should receive only the hash of the commit to update from."

    commit = sys.argv[1]
    print(
        f"Starting timestamp update process for posts changed since commit {commit}"
    )
    main(commit)
