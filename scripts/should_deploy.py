"""
Check whether we should redeploy or not
Takes the previous commit hash as an argument
"""

import os
import sys

import git


def triggers_update(file_path: str) -> bool:
    return not "posts/drafts" in file_path


def main(commit: str) -> bool:
    changes = git.changed_files_since(commit)
    print(f"These are all the changed files:\n{changes}")
    return any(triggers_update(f) for f in changes)


if __name__ == "__main__":
    assert len(
        sys.argv
    ) == 2, "I did not get the right amount of arguments. I should receive only the hash of the commit to update from."

    commit = sys.argv[1]

    print(f"Checking whether we should deploy for changes since {commit}")
    if main(commit):
        print(
            "Based on the changes, I have decided to go ahead with a deploy.")
        sys.exit(0)
    print("I will not deploy")
    sys.exit(1)
