"""
A script for moving a file and its related images into a folder.

Should copy the file and all of hero, twitter-image, og-image, that are present.
When copying the files, it should also rewrite the paths to reference the new locations.
"""

import os
import sys
import shutil
from pathlib import Path

import hakyll

def main(target_root_dir, img_dir, post_path):
    # create target dir (post path basename)
    print(f"Moving {post_path} to {target_root_dir}")
    target_dir = target_root_dir / Path(post_path.stem)
    print(f"using {target_dir} as the new post directory")
    target_dir.mkdir(exist_ok=True)

    # copy org file,
    new_file = Path(shutil.copy(post_path, target_dir))
    print(f"copied {post_path} to {new_file}")

    # copy images (transform /images to ./images)
    keys = ['twitter-image', 'hero', 'og-image', 'hero-fallback']
    metadata = hakyll.unquote_values(hakyll.get_metadata(new_file))
    images = {k: img_dir / Path(v).name for k, v in {k: metadata.get(k) for k in keys}.items() if v}
    print(f"Found images: {images}")
    for img in images.values():
        print(f"Copying {img} to {target_dir}")
        Path(shutil.copy(img, target_dir))


    # update org metadata for images
    for k, v in images.items():
        print(f"Updating {k}. New path: {v.name}")
        hakyll.update_metadata_field(k, v.name, new_file)


    # update metadata format
    print(f"Updating metadata format for {new_file}")
    hakyll.update_metadata_format(new_file)


if __name__ == '__main__':
    assert len(sys.argv) == 4, "I need a target directory, an image source directory, and a source directory"

    _, target_dir, img_dir, source_dir = [Path(x) for x in sys.argv]
    assert source_dir.is_dir(), f"{source_dir} is not a directory"
    assert img_dir.is_dir(), f"{img_dir} is not a directory."
    if not target_dir.exists():
        target_dir.mkdir(parents=True)

    assert target_dir.is_dir(), f"{target_dir} is not a directory."

    paths = [source_dir / Path(p) for p in os.listdir(source_dir) if os.path.isfile(os.path.join(source_dir, p))]
    print(f"Operating on these files: {paths}")

    for p in paths:
        main(target_dir, img_dir, p)
