"""
A script for migrating from Hakyll-format org files with YAML-style
frontmatter, to files with org file variables.
"""

import os
import sys

import hakyll

if __name__ == '__main__':
    assert len(sys.argv) == 2, "I need a post to update"

    _, post_path = sys.argv
    assert os.path.isfile(post_path), f"{post_path} is not a file."

    hakyll.update_metadata_format(post_path)
