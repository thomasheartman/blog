"""
Shared string operations
"""


def strip_surrounding_quotes(s):
    if s.startswith('"') and s.endswith('"'):
        return s[1:-1]
    return s
