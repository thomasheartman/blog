"""
Shared hakyll-related functions
"""

import os
import re

import string_ops

SEPARATOR = "---"


def get_metadata(post_path: str) -> {str, str}:
    with open(post_path, "r") as f:
        num_separators = 0
        is_separator = lambda s: s == SEPARATOR
        metadata_dict = {}
        for line in f:
            l = line.strip()
            if not l:
                continue

            if is_separator(l):
                num_separators += 1
                if num_separators >= 2:
                    break
            elif num_separators == 0:
                break
            else:
                k, v = l.split(":", 1)
                metadata_dict[k] = v.strip()
        return metadata_dict


def is_published(post_path: str) -> bool:
    return os.path.isfile(post_path) and "published" in get_metadata(post_path)


def replace_metadata(content: str, new_metadata: {str, str}) -> str:
    unquote_values(new_metadata)
    fix_tags(new_metadata)
    quote_values(new_metadata)
    metadata = '\n'.join(sorted([f"{k}: {v}"
                                 for k, v in new_metadata.items()]))
    return re.sub("^---$.+?^---$",
                  rf"---\n{metadata}\n---",
                  content,
                  flags=re.DOTALL | re.MULTILINE,
                  count=1)

def replace_metadata_org_format(content: str, new_metadata: {str, str}) -> str:
    unquote_values(new_metadata)
    fix_tags(new_metadata)
    metadata = '\n'.join(sorted([f"#+{k.upper().replace('-', '_')}: {v}"
                                 for k, v in new_metadata.items()]))
    return re.sub("^---$.+?^---$",
                  metadata,
                  content,
                  flags=re.DOTALL | re.MULTILINE,
                  count=1)

def update_metadata_format(path: str):
    with open(path) as source:
        content = source.read()
    updated = replace_metadata_org_format(content, get_metadata(path))
    with open(path, "w") as f:
        f.write(updated)

def update_metadata(path: str, metadata: {str, str}):
    with open(path) as source:
        content = source.read()
    updated = replace_metadata(content, metadata)
    with open(path, "w") as f:
        f.write(updated)


def update_metadata_field(field: str, value: str, post: str):
    metadata = get_metadata(post)
    metadata[field] = value
    update_metadata(post, metadata)


def fix_tags(metadata: {str, str}) -> ({str, str}):
    if not "tags" in metadata:
        return metadata
    tags = metadata["tags"]
    metadata["tags"] = ", ".join(sorted(x.strip().lower() for x in tags.split(",")))
    return metadata


def unquote_values(metadata: {str, str}) -> {str, str}:
    for k, v in metadata.items():
        intermediate = string_ops.strip_surrounding_quotes(v)
        unescaped = re.sub(r'\\"', '"', intermediate)
        metadata[k] = unescaped
    return metadata


def quote_values(metadata: {str, str}) -> {str, str}:
    escape_quotes = lambda x: re.sub(r'(?<!\\)"', '\\"', x)

    def transform(a):
        b = escape_quotes(string_ops.strip_surrounding_quotes(a))
        return f'"{b}"' if b else ""

    for k, v in metadata.items():
        if v:
            metadata[k] = transform(v)
    return metadata


def is_new(post_path: str) -> bool:
    return not get_metadata(post_path)["published"]


# unit tests


def assert_f(f, original: str, expected: str):
    dict = {"key": original}
    result = f(dict)
    print(original, "->", result["key"], "Should be:", expected)
    assert result["key"] == expected


def test_unquoting():
    def assert_unquoted(original: str, expected: str):
        assert_f(unquote_values, original, expected)

    assert_unquoted('"a"', 'a')
    assert_unquoted('""', '')
    assert_unquoted('a', 'a')
    assert_unquoted(r'"it unescapes escaped \"quotes\""',
                    r'it unescapes escaped "quotes"')
    assert_unquoted(r'it respects "quotes"', r'it respects "quotes"')


def test_quoting():
    def assert_quoted(original: str, expected: str):
        assert_f(quote_values, original, expected)

    assert_quoted('a', '"a"')
    assert_quoted(r'it respects \"escaped quotes\"',
                  r'"it respects \"escaped quotes\""')

    assert_quoted('', '')
    assert_quoted('\"\"', '')

def test_tag_sorting():
    dict = {"tags": "b, c, a, d"}
    assert fix_tags(dict)["tags"] == "a, b, c, d"

def test_tag_casing():
    dict = {"tags": "B, caT, a, Dog"}
    assert fix_tags(dict)["tags"] == "a, b, cat, dog"
