import React from "react"
import { graphql } from "gatsby"

import PostList from "../components/post-list"
import Layout from "../components/layout"
import SEO from "../components/seo"

const Component = ({ data, pageContext, location }) => {
  const { tag } = pageContext
  const siteTitle = data.site.siteMetadata.title

  const posts = data.posts.nodes.map(p => ({
    ...p,
    ...p.metadata,
    path: p.fields.path,
    published: {
      iso: p.metadata.published,
      pretty: p.metadata.publishedPretty,
    },
    modified: { iso: p.metadata.modified, pretty: p.metadata.modifiedPretty },
  }))

  return (
    <Layout location={location} title={siteTitle}>
      <SEO title={`#${tag}`} />
      <div className="[ content ]">
        <div className="[ heading ]">
          <div className="[ title-wrapper ]">
            <h1 className="[ title ]">{`Posts tagged "${tag}"`}</h1>
          </div>
        </div>
        <PostList posts={posts} />
      </div>
    </Layout>
  )
}

export default Component

export const pageQuery = graphql`
  query($tag: String!) {
    site {
      siteMetadata {
        title
      }
    }

    posts: allOrgContent(
      sort: { fields: [metadata___published], order: DESC }
      filter: { metadata: { published: { ne: null }, tags: { in: [$tag] } } }
    ) {
      nodes {
        fields {
          path
        }
        metadata {
          title
          description
          publishedPretty: published(formatString: "MMMM DD, YYYY")
          published
          modifiedPretty: modified(formatString: "HH:mm, MMMM DD, YYYY")
          modified
          tags
        }
      }
    }
  }
`
