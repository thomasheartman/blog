import React from "react"
import { graphql, Link } from "gatsby"

import LightSep from "../components/light-separator"
import Post, { makeProps } from "../components/post"
import Layout from "../components/layout"
import SEO from "../components/seo"

const Nav = ({
  currentPage,
  numPages,
  nextPageLink,
  prevPageLink,
  firstPageLink,
  lastPageLink,
}) => {
  const OrDisabled = ({ test, url, text }) =>
    test ? (
      <Link to={url}>{text}</Link>
    ) : (
      <span className="disabled-nav">{text}</span>
    )

  return (
    <div className="[ content pagination-navigation ]">
      <OrDisabled test={currentPage !== 1} url={firstPageLink} text="First" />
      <OrDisabled test={prevPageLink} url={prevPageLink} text="Previous" />
      <OrDisabled test={nextPageLink} url={nextPageLink} text="Next" />
      <OrDisabled
        test={currentPage !== numPages}
        url={lastPageLink}
        text="Last"
      />
    </div>
  )
}

const Component = ({ data, pageContext, location }) => {
  const siteTitle = data.site.siteMetadata.title

  const posts = data.posts.nodes.map(p => ({
    ...p,
    ...p.metadata,
    path: p.fields.path,
    published: {
      iso: p.metadata.published,
      pretty: p.metadata.publishedPretty,
    },
    modified: { iso: p.metadata.modified, pretty: p.metadata.modifiedPretty },
    heroData: data.heroes.nodes.filter(
      h =>
        h.base === p.metadata.images.hero?.image ||
        h.base === p.metadata.images.hero?.fallback,
    )[0],
  }))

  return (
    <Layout location={location} title={siteTitle}>
      <SEO
        title={`Blog (${pageContext.currentPage}/${pageContext.numPages})`}
      />
      <div className="[ subgrid ]">
        <div className="[ span-all ]">
          {posts.map(post => (
            <>
              <Post
                {...makeProps({
                  data: { ...data, hero: post.heroData },
                  post,
                  author: post.metadata.author || data.site.siteMetadata.author,
                })}
              />
              <div className="[ post-separator subgrid ]">
                <div className="content">
                  <LightSep />
                </div>
              </div>
            </>
          ))}
        </div>
        <Nav {...pageContext} />
      </div>
    </Layout>
  )
}

export default Component

export const pageQuery = graphql`
  query($skip: Int!, $limit: Int!, $heroes: [String!]!) {
    site {
      siteMetadata {
        title
      }
    }

    heroes: allFile(filter: { absolutePath: { in: $heroes } }) {
      nodes {
        childImageSharp {
          fluid {
            ...GatsbyImageSharpFluid_withWebp
          }
        }
        base
        extension
        publicURL
      }
    }

    posts: allOrgContent(
      sort: { fields: [metadata___published], order: DESC }
      filter: { metadata: { published: { ne: null } } }
      limit: $limit
      skip: $skip
    ) {
      nodes {
        id
        html
        fields {
          slug
          path
        }
        metadata {
          title
          subtitle
          publishedPretty: published(formatString: "MMMM DD, YYYY")
          published
          modifiedPretty: modified(formatString: "HH:mm, MMMM DD, YYYY")
          modified
          hero_contain
          author
          hero_caption
          tags
          images {
            twitter {
              image
              altText
            }
            openGraph {
              image
              altText
            }
            hero {
              image
              fallback
              altText
              attribution
            }
          }
        }
      }
    }
  }
`
