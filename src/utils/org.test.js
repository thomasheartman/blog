import fc from "fast-check"
import { transformLinks, anchorTag } from "./org"

const orgLink = (url, linkText) => `[[${url}][${linkText}]]`

const arb = {
  link: (n = 6) => fc.tuple(fc.webUrl(), fc.lorem(n)),
  paragraph: (n = 9) => fc.lorem(n, true),
  orgAndHtml: () =>
    arb.link().map(([url, text]) => [orgLink(url, text), anchorTag(url, text)]),
}

describe("org link transformations", () => {
  it("transforms links correctly", () => {
    fc.assert(
      fc.property(arb.orgAndHtml(), fc.context(), ([org, expected], ctx) => {
        const actual = transformLinks(org)
        ctx.log(`Expected: ${expected}. Actual: ${actual}`)
        return expected === actual
      }),
    )
  })
  it("transforms links without descriptions", () => {
    fc.assert(
      fc.property(fc.webUrl(), url => {
        const org = `[[${url}]]`
        const expected = anchorTag(url)
        const actual = transformLinks(org)
        return expected === actual
      }),
    )
  })
  it("replaces multiple links correctly within a paragraph", () => {
    fc.assert(
      fc.property(
        arb.orgAndHtml(),
        arb.orgAndHtml(),
        fc.tuple(arb.paragraph(), arb.paragraph(), arb.paragraph()),
        ([org1, html1], [org2, html2], [p1, p2, p3]) => {
          const f = (linkA, linkB) =>
            `<p>${p1} ${linkA} ${p2} ${linkB} ${p3}</p>`
          const input = f(org1, org2)
          const expected = f(html1, html2)
          return expected === transformLinks(input)
        },
      ),
    )
  })
})
