export const anchorTag = (url, text) => `<a href="${url}">${text ?? url}</a>`

export const transformLinks = input =>
  input.replace(/\[\[([^\]]+)\](\[([^[\]]*)\])?\]/g, (_, url, __, text) =>
    anchorTag(url, text),
  )
