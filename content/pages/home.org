#+title: home
#+location: Oslo, Norway
#+flavor: Developer · Adventurer · Rabbit chaser

Hey! I'm Thomas Heartman, a software developer, writer, and speaker. I like tech, training, and trying new things.

This blog serves as a record of my adventures in the software world, of my experiences with code, and of general thoughts and ideas on anything related. There's code snippets, git tips, programming concepts, CLI app recommendations, and more. Enjoy!


* About the blog
  This blog is built with [[https://www.gatsbyjs.org/][Gatsby]]. You can find the source code on [[https://gitlab.com/thomasheartman/blog][GitLab]] if you wanna steal some tricks or just have a little gander.

* Get in touch!
  Like what I'm doing? Spotted a mistake I made? Want to discuss something? I want to hear from you! You can reach me at the email address listed above, or try [[https://twitter.com/thomasheartman		][Twitter]] if you're into that sort of thing. I'll try and respond in a timely fashion.
