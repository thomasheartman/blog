#+DESCRIPTION: In which I discuss my experiences as a mentor at Oslo Legal Hackathon 2019, what I learned about myself, and why I think it's something you should try.
#+HERO: olh19.webp
#+HERO_ALT: A badge that with the Oslo Legal Hackathon logo and the name Thomas Hartmann on top of a hackathon t-shirt.
#+HERO_CAPTION: Physical artifacts
#+HERO_CONTAIN: t
#+HERO_FALLBACK: olh19.png
#+HERO_WEBP: t
#+MODIFIED: 2020-03-15T20:58:36+00:00
#+PUBLISHED: 2019-09-30T06:52:57+00:00
#+SUBTITLE: A look back at Oslo Legal Hackathon
#+TAGS: hackathon, mentoring, oslo legal hackathon
#+TITLE: A mentor's thoughts

I was a mentor at [[https://www.legalhackathon.no/][Oslo Legal Hackathon]] this past weekend, and while I've been to hackathons before, this was my first time participating as a mentor. I wasn't quite sure what to expect, but I felt ready.

And indeed, everything went pretty swimmingly. Well, mostly. There was a shortage of tech people, and I ended up co-mentoring a team consisting solely of people from the legal industry. Not ideal at an event that's supposed to bring legal and tech together, but they made it to the final round by focusing on the things that they could do rather than what they couldn't. But even though I think my team did a terrific job, that's not really what I want to focus on. Rather, I want to talk about the act of mentoring and how it feels different from participating.

* Stepping down
  Admittedly, I like being in charge. I like overseeing what's going on and giving my input on it. I often end up as team lead or in similar positions. Being a mentor is different.

  /Yes/, I could still get an overview of where the team was headed, and /yes/, I could still give my input on it, but in the end, I knew that I had no real say in the matter.

  I'm also very competitive. It's probably one of the main reasons that I work as hard as I do. Sure, I enjoy what I do, but I also want to be the very best. *Like no one ever was*.

  By being slightly divorced from the process and by not actually being on the team, I could take a step back, breathe, and just enjoy seeing them work and the solutions they came up with.

  In fact, I think /that/ might be the most important thing I learned about myself: I enjoy *enabling* a team and seeing them succeed at least as much as I enjoy being on the team. They're two very different, yet very similar experiences.

* For future reference
  To close this all out, then: would I do it again? Yes. No doubt. Not only was it great fun, it was also a fantastic chance to grow my network, both within tech and within the legal industry, and to get to better know my coworkers and fellow mentors.

  I've been trying to come up with some cons, but I haven't really been able to; the event was well organized, the participants and organizers were all nice people, there was food and drinks, and it was just a fun event all around.

  A bit tongue-in-cheek, perhaps, but it felt great being able to come in, give feedback, express some opinions, and then go away and have someone else do the dirty work for you. I think I understand why people want to be managers now.

  And finally, even if I've only been at this for a comparatively small amount of time, I felt that I really had something to contribute to the team, that I could make a difference, that I could add value. So remember: Even if you're not the most decorated or recognized individual, don't sell yourself short! As long as you invest your time and are genuinely passionate about what you do, you'll be a valuable asset to /any/ team, no matter the role.

  So don't quit, don't stop exploring new things, and don't stop making yourself uncomfortable. If you're curious, go for it!
