#+DESCRIPTION: In which I talk about what features I want to see on the Rust roadmap in 2021. Const generics and generic associated types get honorable mentions, but my vote goes to nested OR-patterns and trait aliases.
#+PUBLISHED: 2020-09-27T22:05:45+02:00
#+TAGS: rust
#+TITLE: Rust 2021
#+SUBTITLE: Minor major improvements

This is my response to the Rust Core Team's [[https://blog.rust-lang.org/2020/09/03/Planning-2021-Roadmap.html][2021 Roadmap call for blogs]].

A year ago, [[https://blog.thomasheartman.com/posts/rust-2020][stabilizing slice patterns was at the top of my wish list for Rust 2020]]. I'd been waiting for that for a long time. A few months later, it landed on the stable channel with release 1.42. I don't expect to be quite as lucky time around, but one can dream.

At the outset, I didn't really have any strong wishes for the next year. But when I sat down and thought about it, I came up with two things that I've run into lately that I know are being worked on and that I would love to see finalized.

Besides these two, there's [[https://github.com/rust-lang/rust/issues/44580][const generics]] and [[https://github.com/rust-lang/rust/issues/44265][GATs]]. High-profile features that get a lot of love in the community. The thing about these two, however, is that whenever I read up on them, they seem great and super useful, but I don't think I've actually run into any cases where I need them while programming. So while I hope the work on these continues, I want to turn my attention to two other points: nested OR-patterns and trait aliases.

* Nested OR-patterns

  This was mentioned as an upcoming feature in the post [[https://blog.rust-lang.org/inside-rust/2020/03/04/recent-future-pattern-matching-improvements.html#nested-or-patterns][Recent and future pattern matching improvements]] from the /Inside Rust Blog/ in March. In short, it allows you to nest patterns when pattern matching.

  Imagine you have an ~Option<u8>~ and you want to check whether the possibly contained ~u8~ is ~4~ or ~13~. Without using the ~or_patterns~, you'd have to do something like this:

  #+BEGIN_SRC rust
    fn is_unlucky(n: Option<u8>) -> bool {
        match n {
            Some(4) => true,
            Some(13) => true,
            _ => false,
        }
    }
  #+END_SRC

  Notice that double ~Some~ check? Using the ~or_patterns~ feature, we can simplify this:

  #+BEGIN_SRC rust
    // make sure you're on nightly!
    #![feature(or_patterns)]

    fn is_unlucky(n: Option<u8>) -> bool {
        match n {
            Some(4 | 13) => true,
            _ => false,
        }
    }
  #+END_SRC

  As always, this is a simplified and contrived example, but it can simplify code considerably in some cases. And yes, I'm sure it can make some code even harder to read too, but that's a tradeoff I'm willing to make.

  So where are we on this? There is an [[https://github.com/rust-lang/rust/issues/54883][open tracking issue]] for it. But there seems to be some blocking issues at the moment, including [[https://github.com/rust-lang/rust/issues/72680][one that makes the compiler panic]], so I'm not sure what the status is. There hasn't been any activity on this since July, but I'm hopeful that we might see some progress on this in the coming year.

* Trait aliases

  Another feature that I've wished for at times is trait aliases (thanks to or Igor Aleksanov ([[https://github.com/popzxc][popzxc]]) for [[https://popzxc.github.io/rust-2021][mentioning this in their Rust 2021 post]]). This feature would allow you to refer to a collection of traits with an alias that you choose.

  This is particularly nifty in situations where you use the same set of traits to define trait bounds over a number of functions. Instead of specifying the same combination over and over again, you can give the combination a name and use that name from there on.

  Using Aleksanov's post as inspiration again, we could have something like this (without trait aliases):

  #+BEGIN_SRC rust
    trait MyTrait {}

    fn f<T>(x: &T)
    where
        T: MyTrait + Send + std::fmt::Debug,
    {
        // do something with x here
    }

    fn g<T>(x: &T)
    where
        T: MyTrait + Send + std::fmt::Debug,
    {
        // do something else with x here
    }
  #+END_SRC

  There are a few things about this code that could be improved with trait aliases. Most notably is the fact that the combination of the three traits we're using (~MyTrait~, ~Send~, ~std::fmt::Debug~) is repeated several times. It's not clear from the context whether these are intentionally or accidentally the same. If they are supposed to be the same, then when changing the combination one place, you'd have to remember to change it everywhere else. And while it's not necessarily bad, this combination is fairly verbose.

  By using a trait alias, we can also give this combination of traits a more meaningful and descriptive name. This can help us better communicate to other developers (and to our future selves) why this combination is what it is.

  Using the ~trait_alias~ feature on nightly, we can instead write it like this:

  #+BEGIN_SRC rust
    // again: you need to be on the nightly channel for this
    #![feature(trait_alias)]

    trait MyTrait {}

    trait MyExtendedTrait = MyTrait + Send + std::fmt::Debug;

    fn f<T>(x: &T)
    where
        T: MyExtendedTrait,
    {
        // do something with x here
    }

    fn g<T>(x: &T)
    where
        T: MyExtendedTrait,
    {
        // do something else with x here
    }
  #+END_SRC

  At first, it might only look like we've added a line or two. But by grouping the trait combination under a new alias, we've ensured that the functions will change their trait bounds in lockstep. We have also given the trait combination a more meaningful name, though, admittedly, ~MyExtendedTrait~ isn't my strongest effort.

  Much like with nested OR-patterns, there is a [[https://github.com/rust-lang/rust/issues/41517][tracking issue]] issue for this. There seems to have been some activity back in March, but not much since.

* That's it for now!

  It's easy to just sit back and wish for new features when you're not the one implementing them. I'm very grateful to all the Rust teams for all their efforts, and I trust them wholeheartedly on what features to focus on in the coming year, whether they align with my wish list or not.

  Rust continues to be one of the most exciting programming languages around and with one of the most welcoming communities to boot! As always: To everyone involved with the language, the ecosystem, and the community (and all the other parts that I'm forgetting): thank you very much. I'm looking forward to another year (and many more) with you!
