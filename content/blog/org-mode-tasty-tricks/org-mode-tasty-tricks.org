#+DESCRIPTION: In which we explore some of the most useful features of Emacs' Org mode, including custom TODO keywords, the agenda view, and capture templates. Name-drops literate programming for bonus points.
#+HERO: M-x-org-mode.svg
#+HERO_ALT: The org mode unicorn positioned above some text saying "M-x org-mode".
#+HERO_CAPTION: Your life in plain text.
#+HERO_SVG: t
#+MODIFIED: 2020-05-18T10:06:20+02:00
#+PUBLISHED: 2020-03-09T07:35:46+00:00
#+SUBTITLE: The best thing since last week
#+TAGS: org mode
#+TITLE: Org mode: tasty tricks
#+TODO: TODO(t) REVIEW(r) | DONE(d) CUT(c)

* Intro
  Org mode ([[https://en.wikipedia.org/wiki/Org-mode][Wikipedia]], [[https://orgmode.org/manual/Capture.html#Capture][official website]]) is a powerful tool. On the surface, it looks like a lightweight markup language akin to Markdown, but if you look closer, you'll find that the Emacs major mode is /so/ much more. It's a scheduler, a task manager, a spreadsheet editor, an organizing tool, and a way to do /literate programming/. I've been using the basic functionality of Org for a long time, but I recently took some time to explore the manual and pick up some new tips and tricks.

* Custom TODOs
  One of the easiest features to get started with is the TODO-system. Being able to tag a headline as ~TODO~ or ~DONE~ with a keypress makes it a super easy to get started with task management. I initially used it for logging work at my old job because keeping track of JIRA tickets through a web browser became a bit tedious.

  The basic two-state system works well in a lot of cases, but sometimes you want a little more control. For that, you can create your own set of keywords. The paragraphs below give some examples of how I use it and some basic tips. For more information, consult [[https://orgmode.org/manual/TODO-Items.html#TODO-Items][the manual]].
** Creating and configuring keywords
   One simple, but very powerful feature of Org is that you can define your own set of keywords to use as TODO-states. You can do this on a global level or on the file level. While doing it on the /global level/ might be useful if you always want access to a specific set of keywords, defining your keywords at the /file level/ allows you to work with a custom set of keywords for every file you operate on.

   To define keywords for a file, use the ~#+TODO:~ keyword and list your desired states, using ~|~ as a separator between states that need work and states that that are considered closed. For instance, here's the set of keywords that I'm using for writing this very post. The set indicates whether a heading needs more work, is ready to be reviewed, is done, or if it should be cut:
   #+begin_src org
  #+TODO: TODO REVIEW | DONE CUT
   #+end_src

   Emacs offers you a number of ways to switch the state of an item. By default ~C-c C-t~ cycles through the list. You can also use ~S-<right>~ and ~S-<left>~ (that's ~S~ as in ~super~), to change to the previous and next states, respectively.

   When you have more than two states, switching to a specific state can start to get tricky if the flow isn't necessarily unidirectional. To make it easier to jump to a state, Org also allows you to add a 'hotkey' that you can use to set that state. This is done by putting the desired key in parentheses after the keyword:
   #+begin_src org
  #+TODO: TODO(t) REVIEW(r) | DONE(d) CUT(c)
   #+end_src

   Now, when you invoke the ~org-todo~ command (~C-c C-t~), rather than cycling through states, Emacs asks you to enter one of the assigned keys. Thus, if an item is currently marked as TODO, but you want to move it to REVIEW, the whole command would be ~C-c C-t r~.

   If you want more information assigned to each state, you can also tell Org to log the date and time of when a TODO-state was assigned as well as prompt you to add a note when that happens. As an example, here's the setup I'm experimenting with for recording and tracking blog post ideas:
   #+begin_src org
     #+TODO: IDEA(i!) DRAFT(d!) UPDATE(u@) READY(r!) | PUBLISHED(p!) ONHOLD(o@)
   #+end_src

   The ~!~ after a letter tells Org mode to log the time when this state was entered. The ~@~ tells Org mode that in addition to logging the time, I also want it to prompt me for a little note. With the set above, I want to track the times of all state changes, and when setting the state to ~UPDATE~ or ~ONHOLD~, I also want to log a little note. This helps me remember why I put it in that state: Was there a paragraph that needed rephrasing? Have I got a better way to do things that I want to update a post with? Did I no longer see the post as relevant?

   The ability to log all of these details is great, but it can quickly get messy. For instance, here's what a heading might end up looking like if we add some timestamps and notes to it:

   #+begin_src org
     ** CUT Drawers
     - State "CUT"        from "REVIEW"     [2020-03-08 Sun 20:17] \\
     This is out of scope for now. Consider extracting this into a separate post.
     - State "REVIEW"     from "TODO"       [2020-03-08 Sun 20:16]
   #+end_src

   This can add up fast, so we can make use of what Org calls /drawers/ to keep our file tidy. These are delimited blocks of content that can contain anything but headlines and other drawers. Content put into drawers will be hidden by default (Org collapses it for you), and it will not be expanded by regular visibility cycling. If you want to cycle the visibility of a drawer, place your cursor on the drawer to cycle it.

   Logging to a drawer would change the above example to this:

   #+begin_src org
     ** CUT Drawers
     :LOGBOOK:
     - State "CUT"        from "REVIEW"     [2020-03-08 Sun 20:17] \\
     This is out of scope for now. Consider extracting this into a separate post.
     - State "REVIEW"     from "TODO"       [2020-03-08 Sun 20:16]
     :END:
   #+end_src

   Now, no matter how many more entries you add to the log, Org will just show you a collapsed ~:LOGBOOK:~ line (that you can expand if you want to). To make Org log into drawers, use the following line:
   #+begin_src org
     #+PROPERTY: LOG_INTO_DRAWER t
   #+end_src


* Org-agenda
  Another part of Org that has great synergy with TODO-items is the agenda view. To get started with agenda, you can add a file to the agenda list with ~org-agenda-file-to-front~ (~C-c [~). Then, when you invoke Org Agenda, you can choose between a number of different ways to view data found in all your agenda list files.

  I'm still getting into it, but I've found immediate benefit from being able to quickly get an overview of all my open TODO items, and being able to easily toggle their states from a unified view, no matter which file they're located in.

  For more information, see [[https://orgmode.org/manual/Agenda-Views.html#Agenda-Views][the chapter on agenda views in the Org Mode manual]].

* Org capture templates
  Capture templates allow you to quickly and easily record notes and ideas into predefined files using a format you specify yourself. This is great for when you get little ideas that you just need to jot down somewhere and store for later.

  I use it to record post ideas and to track the state of certain processes at work. The template allows you to automatically add plenty of context and to prompt the user for specific keywords, making the recording process as painless as you want to. Again, the [[https://orgmode.org/manual/Capture.html#Capture][manual is the best place to go]] for more information.

* Literate programming
  One aspect of Org mode that I'm really excited about but haven't really gotten into yet is [[https://en.wikipedia.org/wiki/Literate_programming][/literate programming/]]. In short, literate programming is a way of intermingling source code blocks and natural language to be able to express intent and explain choices taken along the way.

  There is some documentation on it [[https://orgmode.org/worg/org-contrib/babel/intro.html#literate-programming][on the org mode website]], and I have done some very limited experiments with /tangling/ and /weaving/. Writing an Emacs config from the ground up should be a perfect use case for it, as should any upcoming posts that contain code examples that can be compiled into a standalone program.

* Closing thoughts
  This is just scratching the surface of what Org mode can do. There is so much I haven't covered here, including working with tables, org-babel, tags, exporting files, and so forth; but even with just this, Org mode is already a force to be reckoned with.

  I can see a bright future where my life is nothing but plain text. 🦄
