#+DESCRIPTION: In which I formally deprecate thomashartmann.dev in favor of blog.thomasheartman.com and urge any readers wishing to stay abreast of my writings to subscribe to the new RSS feed.
#+PUBLISHED: 2020-07-12T20:47:25+02:00
#+TAGS: site
#+TITLE: New domain: blog.thomasheartman.com
#+SUBTITLE: Claiming that sweet .com

This is a heads-up to anyone subscribed to thomashartmann.dev's RSS feed: I recently acquired the 'thomasheartman.com' domain and have moved my blog over to [[https://blog.thomasheartman.com][blog.thomasheartman.com]]. The new RSS feed can be found at [[https://blog.thomasheartman.com/rss.xml][blog.thomasheartman.com/rss.xml]].

If you have been reading my posts on my website recently, you should have been silently redirected to the new domain. With this post, this behavior is now made explicit. If you're reading this on the web (as opposed to in some sort of feed reader), you should also find that the URL says blog.thomasheartman.com, regardless of which site you accessed.

I'll leave thomashartmann.dev up and running to catch and redirect any potential traffic to blog.thomasheartman.com, but once this post is up, nothing else will get published to the former.
