#+DESCRIPTION: In which I expand a little bit upon async functionality in Rust, introducing attributes that allow you to run async functions in your main function.
#+HERO: async-main-function.svg
#+HERO_ALT: An 80's style cyberscape with the words "async/.await" printed in the distance. There are a set of glowing eyes in the background and a shadow crab with glowing eyes on the right.
#+HERO_CAPTION: New and improved.
#+HERO_SVG: t
#+MODIFIED: 2020-03-15T20:58:36+00:00
#+PUBLISHED: 2019-11-25T21:17:59+00:00
#+SUBTITLE: Improving async ergonomics
#+TAGS: rust
#+TITLE: Async Rust: async main

In [[https://blog.thomasheartman.com/posts/async-rust/][the previous post]] we had an introductory glance at ~async/.await~ in Rust, looking at what it does and how you'd use it. However, the little bit of code we were left with at the end still felt a bit rough. Coming from languages where you can have an async ~main~ function (or at least one that lets you act as if it is), it felt like an extra hurdle to have to extract all async functionality out into a separate function.

Luckily, Reddit user [[https://www.reddit.com/user/mbuesing/][mbuesing]] [[https://www.reddit.com/r/rust/comments/dyat19/async_rust_a_gentle_introduction/f813vqi?utm_source=share&utm_medium=web2x][pointed out]] that there is an attribute available in async-std that you can use to make your ~main~ function asynchronous: ~async_std::main~! Let's have a look at what changes we'd have to make to incorporate that.

First off, let's update the ~Cargo.toml~ file. It's mostly the same as last time, but we're going to have to add the "attributes" feature from async-std:
#+begin_src toml
  [package]
  name = "async-basics"
  version = "0.1.0"
  authors = ["Your Name <your.email@provider.tld>"]
  edition = "2018"

  [dependencies]
  async-std = { version = "1", features = ["attributes"] }
  surf = "1"
#+end_src

Next up, let's update the ~main.rs~ file. If we're doing everything within the ~main~ function, we can cut it down to about 8 lines of code, compared to the 14 lines we had last time, and because it's such a simple program, it's not any less readable:
#+begin_src rust
  use surf;

  #[async_std::main]
  async fn main() {
      match surf::get("https://pokeapi.co/api/v2/move/surf").recv_string().await {
          Ok(s) => println!("Fetched results: {:#?}", s),
          Err(e) => println!("Got an error: {:?}", e),
      };
  }
#+end_src

-----

So there you have it. With some extra attributes, we can make async code pretty ergonomic in Rust. Now, let's make something cool!
