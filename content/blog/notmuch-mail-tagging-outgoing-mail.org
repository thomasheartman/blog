#+DESCRIPTION: In which we explore the mysteries of notmuch tagging outbound mail as 'unread' and 'inbox' (as if we haven't just written and sent it *out*), and find a simple solution after more effort than we expected.
#+PUBLISHED: 2020-07-12T15:58:48+02:00
#+TAGS: notmuch, emacs
#+TITLE: Notmuch: (un)tag outgoing mail

I'm here to save past me from spending hours searching the internet for
information on how to make notmuch (the Emacs client, specifically) not apply
default tags to outgoing mail.

* The problem
  So you've managed to set up [[https://www.offlineimap.org/][OfflineIMAP]] to fetch mail from your server and
  you've set up [[https://marlam.de/msmtp/][msmtp]] to be able to send mail. It's taken hours (many more than
  you care to admit), but you've finally got a functioning email client directly
  embedded in Emacs! This is a good day! You celebrate by firing off an email to
  another email account you've got set up and ... /what's this?/ You've got an
  unread email from yourself in your inbox?

  On closer inspection, you realize it's the one you just sent off. But why is
  it in your inbox? And why is it tagged 'unread'?

  #+begin_aside
  Notmuch is a tag-based email system. Rather than folders, everything is done
  through tagging, filtering, and searching. As such, your 'inbox' is just all
  mail that's tagged 'inbox'.
  #+end_aside

  You check your ~.notmuch-config~ and find that you've told notmuch to
  automatically tag new mail (pulled from the server) with 'inbox' and 'unread'.
  This makes sense, you think to yourself. After all, you'd like to get new mail
  into your inbox and you'd like it to be marked as unread. But why would
  notmuch apply the same tags to outgoing mail?

* The solution
  After spending hours trying to make notmuch's post-insert hook work (and still
  not quite understanding that one), you decide to check out where sent mail is
  stored, thinking you could use the location to filter the email that gets
  processed by the ~[new]~ section of your config. You remember reading that it
  relates to the 'Fcc' field, so you check out the description for the variable
  ~notmuch-fcc-dirs~ and find this nugget:

  #+begin_quote
  [...] the header should be of the form "folder +tag1 -tag2" where folder is
  the folder (relative to the notmuch mailstore) to store the message in, and
  tag1 and tag2 are tag changes to apply to the stored message.
  #+end_quote

  Can it really be that simple? Is that really it?! All these hours for /that/?
  You set it to ~sent -unread -inbox~ and fire off an email.

  ...

  *Yup*. That's it. Your message template now looks like this and behaves the
   way you want it to, both removing 'unread' and 'inbox' tags /and/ adding a
   'sent' tag so you can easily see all your sent mail:
   #+begin_src sh
From: Your Name &lt;yourname@provider.tld&gt;
To:
Subject:
Fcc: sent +sent -unread -inbox
--text follows this line--
   #+end_src
