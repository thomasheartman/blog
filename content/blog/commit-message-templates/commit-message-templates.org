#+DESCRIPTION: In which we take a closer look at using commit message templates to increase our productivity by decreasing the mental overhead required to write a message. We look at how to set up templates, how to unset them, and a couple of ideas on what to put in them.
#+HERO: commit-message-templates.svg
#+HERO_ALT: The git logo with the title of the article superimposed next to it.
#+HERO_CAPTION: Removing paper cuts
#+HERO_CONTAIN: t
#+HERO_SVG: t
#+MODIFIED: 2020-03-15T20:58:36+00:00
#+PUBLISHED: 2019-09-09T03:53:29+00:00
#+SERIES: git gud
#+SUBTITLE: Why and how
#+TAGS: git
#+TITLE: Commit message templates

Here's a statement for ya: if you're working on a project with someone else, you /should/ be using a commit message template. Not only will it help increase the consistency and usefulness of your messages, but also, and perhaps more importantly, it will reduce the cognitive load required to write a message and the time it takes to write it. I can only speak for myself, but I know that having to go look up information about how a commit message should be structured and what it references causes some much unneeded context switching, resulting in a less efficient workflow.

* Setup

Setting up commit message templates requires only a few steps:

1. Create a template.

   You can use any text file as a template; just put what you want to show up in the commit message buffer in there. Plain lines of text will show up as content, while lines starting with the comment character (~#~ by default) show up as comments.

2. Add it to the git config as a commit template.

   #+begin_src sh
    git config commit.template <path/to/file>
   #+end_src

3. Done! Now try committing something.

-----

When doing this, you're assigning the template locally (to the current repo). If you'd rather do it globally, pass the ~--global~ option.

If you want to share the template across multiple projects in a certain subdirectory, you can do that by combining the template with git's functionality for conditional includes. Be aware, however, that given a relative path to a file, git will look for the file relative to the including configuration's location. While this can potentially be exploited to use different templates for different projects, I've not found the need for it thus far, and have stuck with a single template with an absolute path.

If you want to unset the template, use the ~git config --unset~ functionality:
   #+begin_src sh
    git config --unset commit.template
   #+end_src


* Commit template ideas


Different teams and projects have different needs when it comes to commit messages, so there's no 'one size fits all', but here's a few ideas based on message formats in teams I've been a part of.

** Label listing
   If you label your commits with the type of work they contain to and have a defined set of labels (e.g. 'feature', 'bug', 'chore', 'refactor', etc.), it might be a good idea to list all the different options in the template, so that you can look through them whenever you're committing some code. List them using commented lines and you don't even have to delete them; they'll just show up as extra info in the commit buffer.

   While experienced members of the team are likely to know these by heart, it's very helpful for a new person to have the list right there in the commit buffer so that they don't have to go look them up.

** Issue identification
   If the standard says to have an issue/ticket ID listed in the commit message, you could put a little placeholder in the template that tells you to replace it with the relevant ID.

   If you also work with feature branches and name them according to the feature/issue ID that they correspond to, you could even have git fill in this issue number automatically by using hooks, as explained in my previous post. I have found this to save me a surprisingly large amount of time and mental overhead.

** Very specific formats
   For when your message needs to match a very specific format and it's hard to remember exactly what goes where, templates are a perfect solution.

   For instance, if you're working with a combination of the above two, where you want both the label /and/ the ID, and you want them in a specific order, maybe even with a specific separator (/seems like a hassle, but just roll with it/), you could create a template that you could just fill in with the relevant parts, saving you having to look up what the format is every time.
