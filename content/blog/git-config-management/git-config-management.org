#+DESCRIPTION: In which we dive into git config management with a short overview of the format, and of where git searches for config files by default. We look at how to set values, unsetting them, inspecting them, and how to list all your config values. Plus: a neat trick to debug config collisions!
#+HERO: git-config-management.svg
#+HERO_ALT: The git logo with the title of the article superimposed next to it.
#+HERO_CAPTION: Tie up your loose ends!
#+HERO_CONTAIN: t
#+HERO_SVG: t
#+MODIFIED: 2020-03-15T20:58:36+00:00
#+PUBLISHED: 2019-09-16T07:00:10+00:00
#+SERIES: git gud
#+SUBTITLE: Setting, unsetting, and forgetting
#+TAGS: git
#+TITLE: Git config management

We've been looking a lot at how to tweak git to your liking, which is usually done through your git config file. Sometimes, though, you'll find that the configuration isn't working as you wanted, so I thought it'd be a good idea to look at how you can inspect, unset, and list configuration values from the command line.

Before we dive in, let's have a few words about how your git config files work. Your config files use a specific format which, according to the [[https://git-scm.com/docs/git-config#_configuration_file][docs]], "consists of sections and variables. A section begins with the name of the section in square brackets and continues until the next section begins." That's enough for what we need right now, but go read up if you're interested.

Furthermore, git config files 'cascade', and git, by default, searches four locations on your system, where later entries override earlier ones:
- ~/etc/gitconfig~ :: The system wide config
- ~$XDG_CONFIG_HOME/git/config~ :: Falls back to ~$HOME/.config/git/config~ if ~$XDG_CONFIG_HOME~ is not set or empty.
- ~~/.gitconfig~ :: Your user-specific, 'global' config file
- ~$GIT_DIR/config~ :: Repo-specific config file

Again, see [[https://git-scm.com/docs/git-config#FILES][the specific section of the documentation]] for more information.

One last thing: All setting and unsetting of values apply only to your user configurations, and they are local---repo-specific---by default. To make them apply to your global config, pass the ~--global~ flag.

* Setting values
  To set a value, simply run a command on the form
  #+begin_src sh
    # default (local)
    git config <section>.<key> <value>
    # global
    git config --global <section>.<key> <value>
  #+end_src
  For instance, to change the value of the ~commentChar~ entry of the ~core~ section to ~;~:
  #+begin_src sh
    git config core.commentChar ';'
  #+end_src

* Unsetting values
  Similarly, if you want to unset a value, use the ~--unset~ flag:
  #+begin_src sh
    git config --unset <section>.<key>
  #+end_src

  So if you have overridden the ~core.commentChar~ value, this is the command to undo that and reset it to the default value, ~#~:
  #+begin_src sh
    git config --unset core.commentChar
  #+end_src

* Inspecting config values
  Sometimes, you want to know what a value has been set to. For this, pass the ~--get~ flag and the section and key of the value you want to look up:
  #+begin_src sh
    git config --get <section>.<key>
  #+end_src

  So if you're wondering what your user's name is, for instance:
  #+begin_src sh
    git config --get user.name
  #+end_src


  A few things to note about getting the values
  - Git will show you the last value it finds in the chain :: So if you set your username in your global config, but set a different one in a repo and invoke this command from the repo, it would show you only the repo-specific one.
  - The output will be empty if you have not explicitly set the value :: In other words, if it's not in your config files, git won't show it to you. So if you're looking to inspect git's default values, that's not going to work.

* Finding the config value sources
  Sometimes you just want to inspect what values you're setting in your config. You can do this by passing the ~--list~ flag:
  #+begin_src sh
    git config --list
  #+end_src

  By default, this will just throw all your configuration at you, which isn't all that helpful. However, it takes a pretty handy flag, ~--show-origin~, which lists your configuration in two columns, with the first one being the file in which it was set and the second being the the key-value pairs.

  If you ever find that some value isn't taking effect the way you expect, this is a great way to check what values are getting set in what locations:
  #+begin_src sh
    git config --list --show-origin
  #+end_src

  For instance, if you've changed your global user name, but it's not taking effect in the current repo, you could pipe it into ~grep~ and look for duplicates:
  #+begin_src sh
    git config --list --show-origin | grep "user.name="
  #+end_src
