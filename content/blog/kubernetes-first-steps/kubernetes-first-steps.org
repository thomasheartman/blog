#+DESCRIPTION: In which provision my first managed Kubernetes cluster and suddenly find myself without any idea of what to do next. More navel gazing than usual, this post is largely about trying to find out what I want to do with this beast of a system.
#+HERO: k8s-baby-steps.svg
#+HERO_ALT: The kubernetes logo, next to which is some text saying "kubernetes: analysis paralysis". In the lower right corner is this website's logo.
#+HERO_CAPTION: You know, that's a pretty nice blue.
#+HERO_SVG: t
#+MODIFIED: 2020-03-15T20:58:36+00:00
#+PUBLISHED: 2020-02-23T21:20:58+01:00
#+SERIES:
#+SUBTITLE: Analysis paralysis
#+TAGS:
#+TITLE: Kubernetes first steps

One of my stated [[https://blog.thomasheartman.com/posts/goodbye-2019-hello-2020/][goals for this year]], was to have a Kubernetes cluster running /somewhere/. As stated in the goals post, the deadline for this was April 1^{st}. As of Saturday night, I've got one.

I ended up using [[https://www.digitalocean.com/][Digital Ocean]] for this, taking advantage of the [[https://changelog.com/podcast][Changelog podcast's]] [[https://do.co/changelog][sponsored signup offer]], giving  me two months to spend a $100 credit. While I've heard a lot of good things about Digital Ocean being very affordable, their managed Kubernetes offering can easily rack up some costs if you don't take care.

But now that I've got the cluster up and running, I'm at a bit of a loss. Just where do I go from here? What's the next thing to do? What should I prioritize? And why didn't I just start off with Minikube?

* Why didn't I just use Minikube for now?

  Actually, let's start with the last question: Why didn't I just use Minikube? Honestly, it was mostly a moment of weakness. I was actually busy procrastinating when I thought, 'hmm, I wonder how long it'd take to get set up with Digital Ocean'.

  In hindsight: sure, Minikube would have been cheaper (at least once my credit is up), but having the actual cluster feels more 'real'. Like it's something I need to take care of. It's also available from anywhere and doesn't use my machine's processing power.

* What do I want to get out of this?

  So I got the cluster. Now what? Well, /why/ did I want to do this?

  While the option to just spawn little demo applications and have them available from wherever  is great, I'm more interested in looking at Kubernetes for the overarching architecture and operational side of things. Specifically, I'd like to use this as a way to familiarize myself with:
  - service meshes and API gateways
  - monitoring and telemetry
  - security best practices

  Each of these topics require a great deal of time and practice to master, but I'm not expecting to be a wizened monk any time soon. These are areas that I find fascinating and that I want to explore, but I cannot yet lay out any specific goals, as I need more time to research them.

* Wait, didn't I have another goal for Kubernetes?

  Sure did! In addition to just getting a cluster ready, my second Kubernetes goal was to expose a Haskell app with an API. This is still on the cards, but isn't due any time soon. This leans more towards the Nix and Haskell side of things, and less directly towards Kubernetes, so I can take some time to figure out how I want to do it.

* Now what?

  So where does this leave me? I think the biggest issue I have identified is my lack of knowledge. I've been working with RedHat's OpenShift for the past year, and feel like I have a pretty good grasp of how that works, at least from the dev side. But when faced with that clean cluster, I froze. I didn't know where to go, didn't know what commands to run, or where to turn to for advice. So I need to formulate a plan.

  I think I would like to familiarize myself with Kubernetes more or less from the ground up. That means reading documentation, doing tutorials, and making sure my understanding is correct. Second: I would like to have a look at basic security measures and best practices. At the very least, I want to enable and configure [[https://kubernetes.io/docs/reference/access-authn-authz/rbac/][RBAC]]. When I get this far, I think it would be an appropriate time to take a step back and reevaluate where I'm headed and recalculate.
