exports.makeLinksAbsolute = root => input =>
  input.replace(/href="(\/.+?)"/g, (_, path) => `href="${root}${path}"`)
