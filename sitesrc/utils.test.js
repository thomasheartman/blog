import { makeLinksAbsolute } from "./utils"
describe("absolute links", () => {
  it("transforms multiple links", () => {
    const input = `<dl>
  <dt>Longest post</dt>
  <dd>
    <a href="/blog/lets-read-haskell-programming-from-first-principles-pt-iv"
      >Let's Read Haskell Programming from First Principles part IV</a
    >
  </dd>
  <dt>Shortest post</dt>
  <dd>
    <a href="/blog/rebasing-off-a-repo-root">Rebasing off a repo root</a>
  </dd>
</dl>`

    const siteRoot = "https://my-site.com"
    const expected = `<dl>
  <dt>Longest post</dt>
  <dd>
    <a href="${siteRoot}/blog/lets-read-haskell-programming-from-first-principles-pt-iv"
      >Let's Read Haskell Programming from First Principles part IV</a
    >
  </dd>
  <dt>Shortest post</dt>
  <dd>
    <a href="${siteRoot}/blog/rebasing-off-a-repo-root">Rebasing off a repo root</a>
  </dd>
</dl>`
    expect(makeLinksAbsolute(siteRoot)(input)).toEqual(expected)
  })
})
