// Imports

// [[file:~/projects/blog/sitesrc/metadataTransformer.org::*Imports][Imports:1]]

// Imports:1 ends here

// Fix tags
//    When migrating from Hakyll to Gatsby, I also had to change the file format a
//    bit, trading Hakyll's YAML frontmatter for Org variables. However, one
//    consequence of this was that the way certain metadata got parsed also
//    changed.

//    With Hakyll, tags would get parsed as a comma-separated string, but with
//    Gatsby, it's space-separated. I like being able to have multi-word tags, so
//    we want to change how this is interpreted.

//    Fortunately, it's a fairly simple transformation:
//    #+name: fixTags

// [[file:~/projects/blog/sitesrc/metadataTransformer.org::fixTags][fixTags]]
exports.fixTags = metadata => {
  const tags = (metadata.tags || [])
    .join(" ")
    .split(",")
    .map(x => x.trim().toLowerCase())
    .filter(x => !!x)
  const deduped = [...new Set(tags)]
  deduped.sort()
  return deduped
}
// fixTags ends here

// Implementation

//    We need to be able to parse a line and split it into its separate parts:
//    #+name: split image text

// [[file:~/projects/blog/sitesrc/metadataTransformer.org::split image text][split image text]]
const parseImagePropertyValue = prop => {
  const { image, rest } = prop.match(/^(?<image>[^ ]+)(?<rest>.+)?$/).groups

  const [altText, attribution] = rest
    ? rest.split(" ~~ ").map(x => x.trim() || undefined)
    : [undefined, undefined]

  return {
    image,
    altText,
    attribution,
  }
}

exports.parseImagePropertyValue = parseImagePropertyValue
// split image text ends here

//  When we have parsed image data, we want to turn it into an object.

// #+name: parse image data

// [[file:~/projects/blog/sitesrc/metadataTransformer.org::parse image data][parse image data]]
const parseImageData = (img, alt, altFallback) =>
  !!img
    ? {
        image: img,
        altText: alt || altFallback,
      }
    : null

exports.parseImageData = parseImageData
// parse image data ends here

// #+name: createImageData

// [[file:~/projects/blog/sitesrc/metadataTransformer.org::createImageData][createImageData]]
exports.createImageData = metadata => {
  const {
    hero,
    hero_alt,
    hero_fallback,
    twitter_image,
    twitter_image_alt,
    og_image,
    og_image_alt,
  } = metadata
  const merge = (x, y) =>
    Object.entries(y)
      .filter(([_, v]) => v !== undefined)
      .reduce(
        (acc, [k, v]) => ({
          ...acc,
          [k]: v,
        }),
        x,
      )

  const entriesRaw = Object.entries({
    hero: [hero, hero_alt],
    twitter: [twitter_image, twitter_image_alt],
    openGraph: [og_image, og_image_alt],
  })
    .filter(([_, [img, altText]]) => !!img)
    .map(([key, [img, altText]]) => [
      key,
      merge({ altText }, parseImagePropertyValue(img)),
    ])
    .reduce((acc, [k, v]) => ({ ...acc, [k]: v }), {})

  const entriesProcessed = Object.entries(entriesRaw).reduce(
    (acc, [k, v]) => ({
      ...acc,
      [k]: {
        ...v,
        ...parseImageData(v.image, v.altText, entriesRaw.hero.altText),
      },
    }),
    {},
  )

  if (hero_fallback && entriesProcessed.hero.image) {
    entriesProcessed.hero.fallback = hero_fallback
  }

  return entriesProcessed
}
// createImageData ends here
