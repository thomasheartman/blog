// Tests                                                               :test:
//    :PROPERTIES:
//    :header-args: :tangle metadataTransformer.test.js :comments both :cache yes
//    :END:

//    #+name: imports

// [[file:~/projects/blog/sitesrc/metadataTransformer.org::imports][imports]]
const fc = require("fast-check")
const {
  fixTags,
  createImageData,
  parseImageData,
  parseImagePropertyValue,
} = require("./metadataTransformer")
// imports ends here

// #+name: fixTagTests

// [[file:~/projects/blog/sitesrc/metadataTransformer.org::fixTagTests][fixTagTests]]
describe("fix tags", () => {
  it("handles missing and null tags, and empty lists", () => {
    ;[
      fixTags({ tags: null }),
      fixTags({}),
      fixTags({ tags: ["", ""] }),
    ].forEach(x => expect(x).toEqual([]))
  })
  it("fixes wrong tag separation and sorts input", () => {
    expect(
      fixTags({ tags: ["this,", "is,", "a", "multi-tag,", "post,"] }),
    ).toStrictEqual(["a multi-tag", "is", "post", "this"])
  })
  it("lowercases tags", () => {
    fc.assert(
      fc.property(fc.array(fc.string(15)), strings => {
        const commaSeparated = strings.map(x => `${x},`)
        return fixTags({ tags: commaSeparated }).every(
          x => x === x.toLowerCase(),
        )
      }),
    )
  })
  it("removes duplicates", () => {
    fc.assert(
      fc.property(fc.lorem(1), fc.integer(1, 16), (tag, reps) => {
        const tags = Array(reps).fill(tag + ", ")
        const result = fixTags({ tags })
        expect(result).toStrictEqual([tag])
      }),
    )
  })
})
// fixTagTests ends here

// #+name: test image data creation

// [[file:~/projects/blog/sitesrc/metadataTransformer.org::test image data creation][test image data creation]]
describe("create image data", () => {
  describe("image metadata prop parsing", () => {
    it("handles only an image link", () => {
      fc.assert(
        fc.property(fc.webUrl(1, 10), link =>
          expect(parseImagePropertyValue(link)).toEqual({ image: link }),
        ),
      )
    })
    it("handles an image link and an alt text", () => {
      fc.assert(
        fc.property(fc.webUrl(1, 10), fc.lorem(), (link, altText) =>
          expect(parseImagePropertyValue(`${link} ${altText}`)).toEqual({
            image: link,
            altText,
          }),
        ),
      )
    })
    it("handles an image link and an attribution text", () => {
      fc.assert(
        fc.property(fc.webUrl(1, 10), fc.lorem(), (link, attribution) =>
          expect(parseImagePropertyValue(`${link} ~~ ${attribution}`)).toEqual({
            image: link,
            attribution,
          }),
        ),
      )
    })
    it("handles all properties being set", () => {
      fc.assert(
        fc.property(
          fc.webUrl(1, 10),
          fc.lorem(),
          fc.lorem(),
          (link, altText, attribution) =>
            expect(
              parseImagePropertyValue(`${link} ${altText} ~~ ${attribution}`),
            ).toEqual({
              image: link,
              altText,
              attribution,
            }),
        ),
      )
    })
  })
  describe("image data parsing", () => {
    it("returns the image and data if they are both valid and provided", () => {
      fc.assert(
        fc.property(fc.webUrl(1, 10), fc.lorem(), (img, alt) => {
          expect(parseImageData(img, alt)).toEqual({
            image: img,
            altText: alt,
          })
        }),
      )
    })
    it("returns null if there is no image provided", () => {
      fc.assert(
        fc.property(fc.lorem(), alt => parseImageData("", alt) === null),
      )
    })
    it("uses the fallback alt text if alt text is undefined, null, or an empty string", () => {
      fc.assert(
        fc.property(fc.webUrl(1, 10), fc.lorem(), (image, fallback) => {
          ;[null, undefined, ""].forEach(alt =>
            expect(parseImageData(image, alt, fallback)).toEqual({
              image,
              altText: fallback,
            }),
          )
        }),
      )
    })
  })

  it("returns an empty object if there is no image data", () =>
    expect(createImageData({})).toStrictEqual({}))

  const heroOld = {
    hero: "",
    hero_alt: "",
  }

  const heroNew = {
    hero: "",
  }

  const twitterOld = {
    twitter_image: "",
    twitter_image_alt: "",
  }

  const twitterNew = {
    twitter_image: "",
  }

  const openGraphOld = {
    og_image: "",
    og_image_alt: "",
  }

  const openGraphNew = {
    og_image: "",
  }

  const img = fc.webUrl
  const alt = fc.lorem()
  const imgWithAlt = () => fc.tuple(fc.webUrl(), fc.lorem())

  it("it creates the expected data from old-style metadata", () => {
    fc.assert(
      fc.property(
        imgWithAlt(),
        imgWithAlt(),
        imgWithAlt(),
        (hero, twitter, og) => {
          const metadata = {
            hero: hero[0],
            hero_alt: hero[1],
            twitter_image: twitter[0],
            twitter_image_alt: twitter[1],
            og_image: og[0],
            og_image_alt: og[1],
          }
          expect(createImageData(metadata)).toEqual({
            hero: {
              image: hero[0],
              altText: hero[1],
            },
            twitter: {
              image: twitter[0],
              altText: twitter[1],
            },
            openGraph: {
              image: og[0],
              altText: og[1],
            },
          })
        },
      ),
    )
  })
  it("it creates the expected data from new-style metadata", () => {
    fc.assert(
      fc.property(
        imgWithAlt(),
        imgWithAlt(),
        imgWithAlt(),
        (hero, twitter, og) => {
          const metadata = {
            hero: hero.join(" "),
            twitter_image: twitter.join(" "),
            og_image: og.join(" "),
          }
          expect(createImageData(metadata)).toEqual({
            hero: {
              image: hero[0],
              altText: hero[1],
            },
            twitter: {
              image: twitter[0],
              altText: twitter[1],
            },
            openGraph: {
              image: og[0],
              altText: og[1],
            },
          })
        },
      ),
    )
  })
  it("it creates the expected data from new-style metadata with attribution", () => {
    fc.assert(
      fc.property(
        imgWithAlt(),
        fc.lorem(),
        imgWithAlt(),
        imgWithAlt(),
        (hero, attribution, twitter, og) => {
          const metadata = {
            hero: `${hero.join(" ")} ~~ ${attribution}`,
            twitter_image: twitter.join(" "),
            og_image: og.join(" "),
          }
          expect(createImageData(metadata)).toEqual({
            hero: {
              image: hero[0],
              altText: hero[1],
              attribution: attribution,
            },
            twitter: {
              image: twitter[0],
              altText: twitter[1],
            },
            openGraph: {
              image: og[0],
              altText: og[1],
            },
          })
        },
      ),
    )
  })
  it("opengraph and twitter default to hero alt if none is specified", () => {
    fc.assert(
      fc.property(
        imgWithAlt(),
        fc.lorem(),
        img(),
        imgWithAlt(),
        (hero, attribution, twitter, og) => {
          const metadata = {
            hero: `${hero.join(" ")} ~~ ${attribution}`,
            twitter_image: twitter,
            og_image: og.join(" "),
          }
          expect(createImageData(metadata)).toEqual({
            hero: {
              image: hero[0],
              altText: hero[1],
              attribution: attribution,
            },
            twitter: {
              image: twitter,
              altText: hero[1],
            },
            openGraph: {
              image: og[0],
              altText: og[1],
            },
          })
        },
      ),
    )
  })
  it("handles having having only hero", () => {
    fc.assert(
      fc.property(imgWithAlt(), hero => {
        const metadata = {
          hero: hero[0],
          hero_alt: hero[1],
        }

        expect(createImageData(metadata)).toEqual({
          hero: {
            image: hero[0],
            altText: hero[1],
          },
        })
      }),
    )
  })
})
// test image data creation ends here
