const path = require(`path`)
const { createFilePath } = require(`gatsby-source-filesystem`)
const { fixTags, createImageData } = require("./sitesrc/metadataTransformer.js")

exports.createSchemaCustomization = ({ actions, schema }) => {
  actions.createFieldExtension({
    name: `fixTags`,
    extend() {
      return {
        resolve: fixTags,
      }
    },
  })

  actions.createFieldExtension({
    name: `createImageData`,
    extend() {
      return {
        resolve: createImageData,
      }
    },
  })

  const types = `
    type OrgContent implements Node { metadata: Metadata }
    type Metadata {
      author: String
      tags: [String!]! @fixTags
      images: Images @createImageData
    }
    type Images {
      hero: ImageData
      twitter: ImageData,
      openGraph: ImageData
    }
    type ImageData {
      image: String!
      fallback: String
      altText: String!
      attribution: String
    }
  `

  actions.createTypes(types)
}

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions

  const blogPost = path.resolve(`./src/templates/blog-post.js`)
  const tagsPage = path.resolve(`./src/templates/tag-page.jsx`)
  const blogList = path.resolve(`./src/templates/multi-post-page.jsx`)
  const result = await graphql(
    `
      {
        allOrgContent(
          sort: { fields: [metadata___published], order: DESC }
          filter: { metadata: { published: { ne: null } } }
        ) {
          edges {
            node {
              absolutePath
              fields {
                slug
                path
              }
              metadata {
                title
                tags
                images {
                  hero {
                    image
                    fallback
                  }
                  twitter {
                    image
                  }
                  openGraph {
                    image
                  }
                }
              }
            }
          }
        }
      }
    `,
  )

  if (result.errors) {
    throw result.errors
  }

  const getPostImage = imageType => post => {
    const img = post.metadata.images[imageType]
    return img
      ? [
          ...post.absolutePath.split(path.sep).slice(0, -1),
          (img.fallback || img.image).trimStart(path.sep),
        ].join(path.sep)
      : null
  }

  const getPostHero = getPostImage("hero")

  // Create blog posts pages.
  const posts = result.data.allOrgContent.edges.map(x => ({ ...x.node }))

  posts.forEach((post, index) => {
    const previous = index === posts.length - 1 ? null : posts[index + 1]
    const next = index === 0 ? null : posts[index - 1]

    const [hero, twitter, openGraph] = ["hero", "twitter", "openGraph"].map(x =>
      getPostImage(x)(post),
    )

    createPage({
      path: post.fields.path,
      component: blogPost,
      context: {
        slug: post.fields.slug,
        hero,
        twitter,
        openGraph,
        previous,
        next,
      },
    })
  })

  // create tags pages
  const tags = new Set(posts.flatMap(x => x.metadata.tags))
  tags.forEach(tag => {
    createPage({
      path: `/tags/${tag}`,
      component: tagsPage,
      context: {
        tag,
      },
    })
  })
}

const toUrl = title => {
  const replaceChar = c => {
    const replacement = {
      "'": "",
      ",": "",
      ".": "",
      "!": "",
      "?": "",
      ":": "",
      ";": "",
      "/": "",
      "\\": "",
      " ": "-",
    }[c]
    return replacement === undefined ? c : replacement
  }
  return [...title.trim().toLowerCase()].map(replaceChar).join("")
}

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions

  if (node.internal.type === "OrgContent") {
    const value = toUrl(node.metadata.title)
    createNodeField({
      name: `slug`,
      node,
      value,
    })
    createNodeField({
      name: `path`,
      node,
      value: `/posts/${value}`,
    })
  }

  if (
    node.internal.type === `MarkdownRemark` ||
    node.internal.type === "OrgFile"
  ) {
    const value = createFilePath({ node, getNode })
    createNodeField({
      name: `slug`,
      node,
      value,
    })
  }
}
