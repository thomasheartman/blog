let
  pkgs = import <nixpkgs> { };

  py = pkgs.python3;
  pythonDeps = with py.pkgs;
    with pkgs; [
      py

      # packages
      autoflake
      epc
      flake8
      hy
      importmagic
      isort
      jedi
      nose
      yapf
    ];

  nodeInputs = with pkgs; [ nodejs-14_x yarn ];

  runScript = pkgs.writeShellScriptBin "run"
    "(cd ${toString ./.} && gatsby clean && gatsby develop)";

  buildScript = pkgs.writeShellScriptBin "build"
    "(cd ${toString ./.} && gatsby clean && gatsby build)";

  publishScript = pkgs.writeShellScriptBin "publish"
    "python ${toString ./scripts/publish.py} ${toString ./posts} $@";

  updateFormat = pkgs.writeShellScriptBin "update"
    "python ${toString ./scripts/update_format.py} $@";

in pkgs.mkShell {
  buildInputs = pythonDeps ++ nodeInputs
    ++ [ pkgs.libwebp publishScript updateFormat runScript buildScript ];
}
